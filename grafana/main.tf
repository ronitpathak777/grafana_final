module "vpc" {
  source           = "./modules/vpc"
  cidr_block       = var.cidr_block
  instance_tenancy = var.instance_tenancy
  tags             = var.tags
}
module "subnet" {
  source              = "./modules/subnet"
  public_sub_cidr     = var.public_sub_cidr
  pub_sub_tags        = var.pub_sub_tags
  av_zone_pub         = var.av_zone_pub
  pub_sub_vpc         = module.vpc.vpc_id
  private_subnet_cidr = var.private_subnet_cidr
  pvt_subnet_tag      = var.pvt_subnet_tag
  av_zone_pri         = var.av_zone_pri
  private_sub_vpc     = module.vpc.vpc_id
}


module "igw-public" {
  source  = "./modules/igw-public"
  igw     = var.igw
  vpc_igw = module.vpc.vpc_id
}
module "route-table" {
  source                  = "./modules/route-table"
  pub_rtb                 = var.pub_rtb
  pub_rtb_route           = var.pub_rtb_route
  public_vpc_rtb          = module.vpc.vpc_id
  igw_gateway             = module.igw-public.aws_internet_gateway
  pub_subnet_id           = module.subnet.public_subnet_id
  nat_id                  = module.nat-gateway.nat_id
  private_route_table_tag = var.private_route_table_tag
  pri_subnet_id           = module.subnet.private_subnet_id[*]
}

module "nat-gateway" {
  source    = "./modules/nat-gateway"
  eip_tag   = var.eip_tag
  subnet_id = module.subnet.public_subnet_id
  nat_tag   = var.nat_tag
}
module "ec2" {
  source                   = "./modules/ec2"
  bastion_instance_type    = var.bastion_instance_type
  bastion_tags             = var.bastion_tags
  bastion_subnet_id        = module.subnet.public_subnet_id
  key_name                 = var.key_name
  prometheus_instance_type = var.prometheus_instance_type
  prometheus_subnet_id     = module.subnet.private_subnet_id[0]
  prometheus_server_tags   = var.prometheus_server_tags
  grafana_instance_type    = var.grafana_instance_type
  grafana_subnet_id        = module.subnet.private_subnet_id
  grafana_server_tags      = var.grafana_server_tags
  prometheus_sg_id         = module.security_group.prometheus_sg_id
  grafana_sg_id            = module.security_group.grafana_sg_id
}

module "security_group" {
  source                     = "./modules/security-group"
  prometheus_vpc_id          = module.vpc.vpc_id
  grafana_vpc_id             = module.vpc.vpc_id
  prometheus_sg_name         = var.prometheus_sg_name
  prometheus_inbound_ports   = var.prometheus_inbound_ports
  prometheus_ingr_protocol   = var.prometheus_ingr_protocol
  prometheus_ingr_cidr_block = var.prometheus_ingr_cidr_block
  grafana_sg_name            = var.grafana_sg_name
  grafana_inbound_ports      = var.grafana_inbound_ports
  grafana_ingr_protocol      = var.grafana_ingr_protocol
  grafana_ingr_cidr_block    = var.grafana_ingr_cidr_block
}



module "nacl" {
  source          = "./modules/nacl"
  nacl_vpc_id     = module.vpc.vpc_id
  protocol        = var.protocol
  rule_no         = var.rule_no
  action          = var.action
  nacl_cidr_block = var.nacl_cidr_block
  from_port       = var.from_port
  to_port         = var.to_port
  nacl_tag        = var.nacl_tag
  subnet_id       = module.subnet.private_subnet_id[*]
}

module "ASG" {
  source                     = "./modules/ASG"
  grafana                    = var.grafana
  image_id                   = var.image_id
  instance_type              = var.instance_type
  key_grafana                = var.key_grafana
  grafana_sg_id              = module.security_group.grafana_sg_id
  grafana_asg                = var.grafana_asg
  max_size                   = var.max_size
  min_size                   = var.min_size
  desired_capacity           = var.desired_capacity
  asg_health_check_type      = var.asg_health_check_type
  subnets                    = module.subnet.private_subnet_id
  grafana_asg_scale_up       = var.grafana_asg_scale_up
  adjustment_type            = var.adjustment_type
  scaling_adjustment         = var.scaling_adjustment
  cooldown                   = var.cooldown
  policy_type                = var.policy_type
  asg-scale-up-alarm         = var.asg-scale-up-alarm
  metric_name                = var.metric_name
  threshold_value            = var.threshold_value
  asg-scale-down             = var.asg-scale-down
  adjustment_grafana_type    = var.adjustment_grafana_type
  grafana_scaling_adjustment = var.grafana_scaling_adjustment
  cooldown_period            = var.cooldown_period
  grafana_policy_type        = var.grafana_policy_type
  asg-scale-down-alarm       = var.asg-scale-down-alarm
  cloudwatch_metric_name     = var.cloudwatch_metric_name
  ec2_threshold_value        = var.ec2_threshold_value

}
