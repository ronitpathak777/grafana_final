data "aws_ami" "ubuntu" {
    most_recent = true
 
    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }
 
    filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }
 
    filter {
        name   = "architecture"
        values = ["x86_64"]
    }
 
    owners = ["099720109477"] # Canonical official
}


resource "aws_instance" "bastion_server" {
    ami           = data.aws_ami.ubuntu.id
    instance_type = var.bastion_instance_type
    subnet_id = var.bastion_subnet_id
    key_name = var.key_name

  tags = {
    Name = var.bastion_tags

}
}

resource "aws_instance" "prometheus_server" {
    ami           = data.aws_ami.ubuntu.id
    instance_type = var.prometheus_instance_type
    subnet_id = var.prometheus_subnet_id
    key_name = var.key_name
    vpc_security_group_ids = [var.prometheus_sg_id]

  tags = {
    Name = var.prometheus_server_tags

}
}

resource "aws_instance" "grafana_server" {
    ami           = data.aws_ami.ubuntu.id
    count = length(var.grafana_server_tags)
    instance_type = var.grafana_instance_type
    subnet_id = var.grafana_subnet_id[count.index]
    key_name = var.key_name
    vpc_security_group_ids = [var.grafana_sg_id]

  tags = {
    Name = var.grafana_server_tags[count.index]

}
}
