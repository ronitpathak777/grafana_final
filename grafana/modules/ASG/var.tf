variable "grafana" {
  type =   string
  description = "template name"  
}
variable "image_id" {
  type = string
  description = "launch_ami"  
}
variable "instance_type" {
  type = string
  description = "instance"  
}

variable "key_grafana" {
  type = string
  default = "grafana" 
}
variable "grafana_sg_id" {
  type = string
  description = "grafana_sg_id"  
}
#####################asg########################
variable "max_size" {
  default = "4"
  description ="max size"
}
variable "min_size" {
  default = "2"
  description ="min size"
}
variable "desired_capacity" {
   default = "2"
  description ="description size"
}
variable "asg_health_check_type" {
   default = "EC2"
  description ="health check of instance"
}
variable "grafana_asg" {
  default = "grafana"
  description = "security_group name"
}
variable "grafana_asg_scale_up" {
  default = "grafana_asg_scale_up"
}
variable "adjustment_type" {
  default = "ChangeInCapacity"
}
variable "scaling_adjustment"{
  default = "1"
}
variable "cooldown" {
  default = "300"
}
variable "policy_type" {
  default = "simple scaling"
}
variable "asg-scale-up-alarm" {
  default = "grafana-scale-up"
}
variable "metric_name" {
  default = "120"
}
variable "threshold_value" {
  default = "30"
}
variable "subnets" {
  type    = list(string)
  default = ["grafana-sub-01", "grafana-sub-02", "grafana-sub-03"]
}

####################### scale down ############
variable "asg-scale-down" {
  default = "grafana_asg_scale_down"
}
variable "adjustment_grafana_type" {
  default = "ChangeInCapacity"
}
variable "grafana_scaling_adjustment" {
  default = "-1"
}
variable "cooldown_period" {
  default = "300"
}
variable "grafana_policy_type" {
  default = "SimpleScaling"
}
variable "asg-scale-down-alarm" {
  default = "asg-scale-down"
}
variable "cloudwatch_metric_name" {
  default = "CPUUtilization"
}
variable "ec2_threshold_value" {
  default = "5"
}


