variable "prometheus_vpc_id" {
   description = "specify the security group name here"
   type = string
}
variable "grafana_vpc_id" {
   description = "specify the security group name here"
   type = string
}
variable "prometheus_sg_name" {
   description = "specify the security group name here"
   type = string
}
variable "prometheus_inbound_ports" {
   description = "specify the inbound ports here"
   type = list(string)
}
variable "prometheus_ingr_protocol" {
   description = "specify the ingress protocol here"
   type = string
}
variable "prometheus_ingr_cidr_block" {
   description = "specify the ingress cidr block here"
   type = list(string)
}
variable "grafana_sg_name" {
   description = "specify the security group name here"
   type = string
}
variable "grafana_inbound_ports" {
   description = "specify the inbound ports here"
   type = list(string)
}
variable "grafana_ingr_protocol" {
   description = "specify the ingress protocol here"
   type = string
}
variable "grafana_ingr_cidr_block" {
   description = "specify the ingress cidr block here"
   type = list(string)
}
