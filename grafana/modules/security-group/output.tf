
output "prometheus_sg_id" {
    value = aws_security_group.sg_prometheus.id
}
output "grafana_sg_id" {
    value = aws_security_group.sg_grafana.id
}

